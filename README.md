<h1>Knihovna Lite a ukázkový program World</h1>
<p class="nastred">autorem JIVA a dalí z internetu</p>

<p>Cílem knihovny Lite je poskytnout rozhraní, jeden celek, nadstavbu nad samotným Borland Pascalem 7.0, se kterým lze jednodue a rychle seskládat programy, které budou v grafickém reimu a umoní přehrávat i zvuky. Například rychle napsat hru, která vypadá naprosto seriózně. Nebo jinak: mohl by to být program dělající cokoliv jiného, ovem chceme vynést na obrazovku například graf nebo informace. Cílem tohoto jednotného celku "na vechno" je, aby si zachoval status "Lite", tzn. odlehčený, malý nebo chcete-li bez cukru.</p>

<p>V současné době je Lite koncipovaný na program pro reálný reimu procesoru. Z volných 640 KB paměti obsadí asi polovinu, moná i víc. Z toho vyplývá, e výsledný program Litu můe nabýt asi tak velikosti 128..192 KB. Pracuje se na verzi pro protected mode, ta bude vyuívat RTM.EXE. Lite se jako projekt pro reálný reim a součást výsledného programu logicky skládá z meních knihoven. Umoní program 150 KB v grafice, se zvuky atd. bez nutnosti dodat jakoukoli dalí knihovnu. Jedná se o programovací sadu, která vznikla sestavením z knihoven různých autorů. Dalími zdroji byly manuálové programy jako SYSMAN, PCGPE nebo ATHELP. Knihovna je tedy sběraniko, je ovem tak zaformátované, e by mělo dávat smysl.</p>


<h3>Podknihovna *1* - GLOB</h3>

<p>V této části se nacházejí základní deklarace popisující paměový model programu (v tuto chvíli real). V souboru Glob, který představuje první podknihovnu tpu, najdeme seznam vech estnácti částí. Kadá část je podknihovna, ve finále se linkují do jednotného Litu, který přesahuje 64 KB. V globu je to rozdělené na veobecně Types, Variables, Const a kadá z těchto částí je dál rozdělená na 16 podčástí. V boku je nadepsán prefix, jména podknihovny, v druhém boku je případný komentář, v těle asi veprostřed jsou definované typy, proměnné a konstanty. 16 - 16 - 16. I v případě, e daná sekce neobsahuje nic, přesto je zadána jako kolonka pro pozdějí moné doplnění.</p>

<p>Níe následuje část, kde zase v 16 sekcích podknihoven najdeme programové monosti. Ty si prohlédněte, to jsou stavební kameny pro výsledný program. Ty a jedině ty. Teoreticky k nim patří samozřejmě jetě programové prvky pascalu samého jaksi bez knihoven. Od toho je to Lite, e je to strohé. Měl by v tom být přehled. Cílem je vdy minimalizace okolních doplňujících variabilit. Co je moné, je dané staticky. To vede ke rychlému ovládnutí rozhraní Lite, prakticky bez nutnosti načerpat obsáhlé téma k rychlé produktivitě grafických programů.</p>

<p>Za Interfacem je Implementation, který se bez výjimek zakládá na include souborech jako v Céčku. Include soubory naleznete ve sloce STORE v podslokách s čísly a zkrácenými jmény. Snail jsem se i o to, aby file browser Borland Pascalu, který zobrazuje dva sloupce, byl optimálně pokryt 8+8 soubory podknihoven a dále 16 slokami ve store. Jednotlivé koncové include funkce by měly být jen tak početné, aby si file browser zachoval schopnost rychlého nalezení a otevření souboru. Kompilace probíhá bat souborem, který má přeloit vechny podknihovny najednou, ale pokud chcete, kompilujte manuálně, postupně od souboru GLOB k dalím podknihovnám. Vechny jsou na sobě závislé navzájem se stoupající tendencí potřeby vech vemi.</p>


<h3>Podknihovna *2* - RAM</h3>

<p>V této podknihovně se snaím o obsluhu vyí paměti. Dokud je knihovna pro real, realizuji to pomocí XMS. Poprvé je vidět, e příkazy mají také prefixy. Je to pro přehled v knihovně, aby bylo zřejmé, e je pouit stavební prvek Litu.</p>


<h3>Podknihovna *3* - DOS</h3>

<p>Tato pasá je zamýlená náhrada DOSU a CRT, která ale není dotaená do konce, je jen v náznaku nejnutnějích funkcí. K čemu to je, kdy je DOS a CRT v základním pascalu. Úprava na bez CRT a bez DOSu povede k odstranění problémů spojených s chybou v některých verzích jednotky CRT. Knihovna Lite by vypadala lépe, kdyby nevyadovala nic, dokonce ani DOS a CRT. Nedokončeno.</p>


<h3>Podknihovna *4* - VGA</h3>

<p>Lite pracuje s VGA fixně v reimu 320x200x256. V GLOBu je připravený paměový prostor Buffers. Definoval jsem 4 Buffery po 64 KB očíslované 0 1 2 3. Jsou to pointery, které ukazují na čtyři bloky 64 KB. Ty jsou přítomné v paměti po celou dobu běhu programu. Vechny se pouijí. Buffer číslo 4 pak představuje VIDEO RAM na segmentu A000h. Práce s VGA je pojata takto: Buffer 0 slouí jako pomocný pro načítání ze souborů. Do Bufferů 1 a 2 se nahrávají kousky grafiky a grafika fontu a pak se procesem přenesení obdélné oblasti kopírují dále do Bufferu 3. Ten představuje finální framebuffer, který je cyklicky vysílán (flip) na monitor (Buffer 4).</p>

<p>Podknihovna umí nakreslit pixel, čáru a prázdný nebo plný obdélník, vymazat nebo zkopírovat celý buffer (včetně synchronizace s obrazovkou - WaitRetrace) a různými způsoby přenáet obdélníky obsahu z Bufferu do Bufferu (celé nebo s vynecháním průhledných pixelů).</p>


<h3>Podknihovna *5* - SVGA</h3>

<p>Stejně jako VGA pracuje s fixním rozliením 640x480, 256 barev. Není o moc pomalejí ne VGA, tzn. průtok dat odpovídá takové rychlosti, e to stačí na větinu úloh a nikde nic nebliká, ovem vzhledem k více ne čtyřnásobnému objemu grafických dat se dostáváme na rychlost pouze 25 FPS a méně. Pouívají se stejné Buffery 0123 jako ve VGA, jsou v programu platné vude jako vymezený prostor. Buffer 4 u kvůli bankování VRAM nemá smysl. Máme monost psát přímo na viditelnou scénu, nebo pouít framebuffer v paměti karty (TogglePaging). Jména a parametry procedur by měly být stejné jako u VGA (Pixel, Rectangle, Bar, Spr, BarZ, SprZ, Clear, Flip, WaitRetrace), jen se lií prefixem SVGA u příkazů.</p>


<h3>Podknihovna *6* - 3DFX</h3>

<p>Program pro DOS je dnes samozřejmě spoutěn velmi často v rozhraní Dosbox 0.74. Tento emulátor dosu disponuje emulací grafického akcelerátoru 3DFX, k jeho pouití pro 3D grafiku 640x480 60 FPS je dodaná ruská Inertia. V přísluné části STORE (sloka 3DFX) se nacházejí soubory knihovny Inertia = profesionální 3D knihovna, která se pouívala v době raných grafických akcelerátorů a pak byla uveřejněna zdarma. Je poměrně obsáhlá. Začlenil jsem ji do Litu tak, aby se přehledně zkompilovala ve verzi pro reálný reim a stala se součástí moností. Je v jednání sestavení příkazů Pixel Rectangle Bar Spr BarZ SprZ Clear Flip WaitRetrace, které by byly postavené na Inertii. Znamenalo by to 60 FPS 640x480 v konvenci Litu.</p>


<h3>Podknihovna *7* - TEXT</h3>

<p>Obsahuje tři druhy grafického vypisování textu a čísel. První OutText píe českým podomácku sestaveným fontem (const). Umí běné znaky, ale se specifičtějím znakem si neporadí. Druhý WriteText píe barevným uivatelským fontem: do Bufferu 1 nebo 2 se nahraje tabulka znaků (příkaz load Default font) a WriteText z ní potom bere jednotlivá písmena a přenáí je na výstup. Je to k tomu, abyste si mohli podle vzorové tabulky sestavit svou vlastní, třeba jako BMP. Změní se jen zdrojový soubor a rozměry znaků. Tabulka je na 26 písmen, 10 čísel a 4 něco, co zbylo (celkem 40 znaků). Třetí Text je Mircosoftův, píe jednobarevně, umoňuje kompletní znakovou sadu a fonty nahrává ze souborů FNT.</p>

<p>Tu uvedu: Sprajty ve VGA a SVGA a tady u textu WriteText pracují tak, e 1 pixel = 1 bajt, stejně jako na obrazovce. Pixely s hodnotou nula se povaují za "průhlednou" černou barvu, která se nekreslí. Jak by tedy vypadalo BMP s fontem? Pozadí bude černé s barvou rovnou nule a jen pixely jiných barev se vykreslí.</p>


<h3>Podknihovna *8* - MAPS</h3>

<p>Mapa je základ pro hru, ale i pro jiné účely. Máme oblast 640x400 pixelů grafiky. Na monitoru se zobrazuje výřez například 100x100 nebo 320x200 a oblast "Mapa" je vyobrazení segmentu v rozliení 320x200 v rozliení 640x480 segmenty a pak v 640x480 celek. Mapa je sestavená z políček 8x8 pixelů (kreslí se přes Spr nebo Bar) a je uloená jako řada čísel 0..499. Tato čísla jsou indexy políček v tabulce spritů. Tabulka je uloena v levé polovině Bufferu 2 (320x200) ve formě obdélníku 160x200 px, tedy 20x25 políček 8x8 px. Index 0 odpovídá políčku v levém horním rohu, dál to pokračuje doprava a pak po řádcích dolů. Mapa tedy umoňuje z modelových kostiček 8x8 v Bufferu 2 sestavovat na obrazovce opakující se vzory a podobně. Mělo by to připomínat dobu Nintendo 8b kazet 128..256 KB.</p>


<h3>Podknihovna *9* - TIME</h3>

<p>Obsahuje Paletu, Speaker a Timer.</p>

<p>Část Timer umonuje Delay (čekání), dále Stopky a proměnnou Second automaticky blikající na frekvenci 1 Hz.</p>

<p>Část Paleta umoňuje přečíst nebo nastavit RGB sloky jednotlivých barev a vybrané úseky palety asynchronně stmívat, rozjasňovat nebo s nimi blikat. Pracujeme po estnáctkách (např. úsek 16 barev 50..65), odtud i prefix.</p>

<p>Část Speaker přehrává zvuky na interním pípátku počítače (PC-Speaker, Dosbox ho emuluje přes reproduktory zvukové karty). Je tu definovaný formát na zvuky: vlna vzorků přejede a vydá to zvuk asi jako kváknutí nebo zakvíknutí. Pak je tu generátor zvuku. Umoňuje vysílat oktávu CDEFGAHC tam a zpět jako houkačku nebo třeba pozměnit frekvence některých not a vytvářet fantomový nízkofrekvenční signál navázaný na hudbě (pouití v psychowalkmanu WORLD).</p>


<h3>Podknihovna *10* - SOUND BLASTER 16</h3>

<p>Přehrává zvuky na zvukové kartě. Jako základ byl pouit Sound Blaster 16 Ethana Brodského, který větinou hraje, a Mircosoftovy Zvuky, které nefungují v Dosboxu. Ve sloce STORE/10-SB16 jsou obsaené utility na práci se zvukem.</p>


<h3>Podknihovna *11* - KEYBOARD 1</h3>

<p>Moje Lite klávesnice, detekce stisku.</p>


<h3>Podknihovna *12* - KEYBOARD 2</h3>

<p>Mirkosoftova klávesnice, komplexní program, kvůli psaní memo v programu WORLD.</p>


<h3>Podknihovna *13* - MOUSE</h3>

<p>Práce s myí, pohyb, stisk.</p>


<h3>Podknihovna *14* - MATH</h3>

<p>Rutiny pro vynáení grafů nebo kreslení objektů hry a jejich pohyb po křivkách. Například kreslení sřely letící po dráze.</p>


<h3>Podknihovna *15* - IPX</h3>

<p>Zcela funkční metoda spojení dvou dosboxových stanic a výměna dat (paketů) po síti.</p>


<h3>Podknihovna *16* - MISC</h3>

<p>Načítání a ukládání datových formátů pro zvuky, grafiku a mapy. A hlavně spoutěcí a ukončovací procedury PowerOn a PowerOff. Ta první se stará o nastartování vech ostatních částí knihovny (inicializace proměnných, alokace Bufferů a podobně), volejte ji hned na začátku programu. Tu druhou volejte před jeho ukončením, postará se o úklid a bezpečné vypnutí.</p>


<hr>


<h2>Ukázkový program WORLD</h2>

<p>Vysoce účinný psychedelický nástroj navozující změněné stavy vědomí. Od lehčího omámení a k silným stavům trvajícím dlouho. Postupujte jen s nejvyí mírou opatrnosti, pouijte nejlepí rozum, jaký máte.</p>
